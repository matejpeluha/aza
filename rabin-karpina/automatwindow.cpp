#include "automatwindow.h"
#include "ui_automatwindow.h"

#include <QCloseEvent>
#include <QInputDialog>
#include <QDebug>


AutomatWindow::AutomatWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AutomatWindow)
{
    this->ui->setupUi(this);
    this->ui->table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->text = "";

    setWindowTitle("Prefixova funkcia");


}

AutomatWindow::~AutomatWindow()
{
    delete ui;
}


void AutomatWindow::closeEvent (QCloseEvent *event)
{
        emit closeAutomat();
        event->accept();
}


void AutomatWindow::on_writeTextButton_clicked()
{
    QString text = QInputDialog::getText(this, tr("Zadaj text"),
                                       tr("Text:"));

    if(!text.isEmpty())
        this->text = text;

    addTextToTable();
}

void AutomatWindow::addTextToTable(){
    int textLength = this->text.size();
    this->ui->table->setColumnCount(textLength);

    for(int index = 0; index < textLength; index++){
        this->ui->table->setItem(0, index, new QTableWidgetItem(QString::number(index)));
        this->ui->table->setItem(1, index, new QTableWidgetItem(this->text.at(index)));
        this->ui->table->setItem(2, index, new QTableWidgetItem(""));
    }
}







void AutomatWindow::on_calculateButton_clicked()
{
    int textLength = this->text.size();
    QString textPreffix, smallPreffix;
    int pi = 0;

    for(int index = 1; index < textLength; index++){
        textPreffix = this->text.left(index + 1);
        for(int preffixIndex = index - 1; preffixIndex >= 0; preffixIndex--){
            smallPreffix = this->text.left(preffixIndex + 1);
            if(textPreffix.endsWith(smallPreffix))
                pi = smallPreffix.size();
        }

        this->ui->table->setItem(2, index, new QTableWidgetItem(QString::number(pi)));

        pi = 0;
    }


     this->ui->table->setItem(2, 0, new QTableWidgetItem("0"));
}
