#ifndef AUTOMATWINDOW_H
#define AUTOMATWINDOW_H

#include <QMainWindow>

namespace Ui {
class AutomatWindow;
}

class AutomatWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AutomatWindow(QWidget *parent = nullptr);
    ~AutomatWindow();

    void closeEvent(QCloseEvent *event);
private:
    Ui::AutomatWindow *ui;
    QString text;

    void addTextToTable();
signals:
    void closeAutomat();
private slots:

    void on_writeTextButton_clicked();
    void on_calculateButton_clicked();
};

#endif // AUTOMATWINDOW_H
