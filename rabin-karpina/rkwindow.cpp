#include "rkwindow.h"
#include "ui_rkwindow.h"
#include <QMessageBox>

#include <QCloseEvent>
#include <QDebug>

RKWindow::RKWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::RKWindow)
{
    ui->setupUi(this);
    setWindowTitle("Rabin-Karp");
}

RKWindow::~RKWindow()
{
    delete ui;
}


void RKWindow::on_pushButton_clicked()
{
    QRegExp re("\\d*");

    QString text = this->ui->searchedWordInput->toPlainText();
    QString longText = this->ui->longTextInput->toPlainText();
    if (!re.exactMatch(text) || !re.exactMatch(longText) || text.isEmpty() || longText.isEmpty()){
        QMessageBox::warning(this, tr("Rabin-Karp"),
                                       tr("Hladane slovo aj prehladavany text musia byt ciselneho charakteru"),
                                       QMessageBox::Ok);
        return;
    }


    int numberOfFalse = 0;
    int q = this->ui->qInputBox->value();
    int length = text.size();
    int numberText = text.toInt();
    int hash = numberText % q;

    QString allFalseTexts;
    QString substring;
    int numberSubstring;
    int substringModulo;
    for(int index = 0; index < longText.size() - length + 1; index++){
        substring = longText.mid(index, length);
        numberSubstring = substring.toInt();
        substringModulo = numberSubstring % q;

        if(substringModulo == hash && numberSubstring != numberText){
            numberOfFalse++;
            allFalseTexts.append(substring + ", ");
        }
    }

    this->ui->numberOfFalseTexts->setText(QString::number(numberOfFalse));


    allFalseTexts.remove(allFalseTexts.size() - 2, 1);
    this->ui->falseTexts->setText(allFalseTexts);

}


void RKWindow::closeEvent (QCloseEvent *event)
{
        emit closeRK();
        event->accept();
}
