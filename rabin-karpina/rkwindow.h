#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class RKWindow; }
QT_END_NAMESPACE

class RKWindow : public QMainWindow
{
    Q_OBJECT

public:
    RKWindow(QWidget *parent = nullptr);
    ~RKWindow();

    void closeEvent(QCloseEvent *event);
private slots:

    void on_pushButton_clicked();

private:
    Ui::RKWindow *ui;

signals:
    void closeRK();
};
#endif // MAINWINDOW_H
