#include "startwindow.h"
#include "ui_startwindow.h"

StartWindow::StartWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StartWindow)
{
    ui->setupUi(this);
    this->rabinKarpinaWindow = new RKWindow();
    connect(this->rabinKarpinaWindow, SIGNAL(closeRK()), this, SLOT(openStartCloseRK()));

    this->automatWindow = new AutomatWindow();
    connect(this->automatWindow, SIGNAL(closeAutomat()), this, SLOT(openStartCloseAutomat()));

    setWindowTitle("AZA");
}

StartWindow::~StartWindow()
{
    delete ui;
    delete this->rabinKarpinaWindow;
    delete this->automatWindow;
}

void StartWindow::on_pushButton_clicked()
{
    this->rabinKarpinaWindow->show();
    this->hide();
}

void StartWindow::on_pushButton_2_clicked()
{
    this->automatWindow->show();
    this->hide();
}


void StartWindow::openStartCloseRK()
{
    this->rabinKarpinaWindow->close();
    this->show();
}

void StartWindow::openStartCloseAutomat()
{
    this->automatWindow->close();
    this->show();
}

