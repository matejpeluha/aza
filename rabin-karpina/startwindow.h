#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include "automatwindow.h"
#include "rkwindow.h"

#include <QMainWindow>

namespace Ui {
class StartWindow;
}

class StartWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StartWindow(QWidget *parent = nullptr);
    ~StartWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::StartWindow *ui;
    RKWindow* rabinKarpinaWindow;
    AutomatWindow* automatWindow;

private slots:
    void openStartCloseRK();
    void openStartCloseAutomat();
    void on_pushButton_2_clicked();
};

#endif // STARTWINDOW_H
